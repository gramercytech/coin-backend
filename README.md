Project (client and project name):

Developer(s):
  Drew Reynolds, Thomas Yancey
Software:
  Ruby on Rails
  redis
  sidekiq
  postgresql
  elasticache
  WAF

Setup Instructions:

1. specify ruby version with RVM or rbenv
2. select node version that works with webpacker, over 10 should be fine
3. bundle install
4. rails s
5. ./bin/webpack-dev-server for hot reloading
6. rails db:create db:migrate
7. check to make sure database is pointing to development, rails db:seed

Hardware:
1. hosted on AWS within client-beanstalks vpc
2. ran on elastic beanstalk, backed by either one or two t2 medium instances
3. use WAF to specify rules for firewall, only certain few ips can access superadmin


Deploy Instructions:

1. make sure that you have ebtools and aws-cli installed, make sure to have your verizon gramercy profile and secret key in your ~/.aws/config
1. Find the elastic beanstalk instance, go into configuration -> security to find the pem key that is in use
2. retrieve pemkey, from root of app run eb deploy, will deploy all committed changes on current branch

Live URL: superadmin.vzpurposecoin.com, api.vzpurposecoin.com

Project Notes:
This is the admin panel and database for the vzpurposecoin.com react app. everything is behind a web application firewall specified in aws WAF console under gramercyonly.
