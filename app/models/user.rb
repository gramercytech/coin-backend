class User < ApplicationRecord
  has_many :transfers, dependent: :delete_all
  has_many :coins, through: :transfers
  has_many :reminders, dependent: :destroy
  validates_presence_of :email
  RESULT_TYPES = ["administrative_area_level_1", "locality"]
  
  has_person_name

  def coin
    coins.try(:first)
  end

  def only_one_coin
    if self.coins.length > 1
      errors.add(:name, "user is already associated with a coin")
    end
  end

  def fetch_location_data
    search = Geocoder.search(self.zip)
    found_data = search[0].data["address"] rescue nil

    if self.city || self.state
      return
    end
    
    if !self.zip || !self.latitude
      return
    end


    
    fetch_str = ""
    if self.zip
      fetch_str = "https://maps.googleapis.com/maps/api/geocode/json?address=#{self.zip}&result_type=#{RESULT_TYPES.join("|")}&key=#{Rails.application.credentials.google_api_key}"
    else
      fetch_str = "https://maps.googleapis.com/maps/api/geocode/json?latlng=#{self.latitude},#{self.longitude}&result_type=#{RESULT_TYPES.join("|")}&key=#{ENV["GOOGLE_API_KEY"]}"
    end

    response = HTTParty.get(fetch_str)

    city = nil
    state = nil

    has_results = response["results"][0]["address_components"] rescue nil
    
    if !has_results
      return
    end

    update_params = {}

    response["results"][0]["address_components"].each {|ac|
      if ac["types"].include?("administrative_area_level_1")
        state = ac["short_name"] || ac["long_name"]
        update_params[:state] = state
      end
      if ac["types"].include?("locality")
        city = ac["long_name"] || ac["short_name"]
        update_params[:city] = city
      end
    }

    self.update(update_params)

  end  
  
end
