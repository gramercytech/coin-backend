require 'csv'

class Transfer < ApplicationRecord
  TRANSFER_ATTRS = ['coin_number', 'user_first_name', 'user_last_name', 'user_email', 'user_zip', 'giver_first_name', 'giver_email', 'notes', 'transfer_date']
  belongs_to :user
  belongs_to :coin
  validate :only_one_coin, if: :should_validate?
  validate :not_same_user, if: :should_validate?
  validate :is_coin_owner, if: :should_validate?
  has_many_attached :files
  has_many :reminders, through: :user





  def self.all_transfers_query
    <<~SQL
    SELECT c.number as coin_number,
    u.first_name as user_first_name, u.last_name as user_last_name, u.email as user_email, u.zip as user_zip, 
    g.first_name as giver_first_name, g.last_name as giver_last_name, g.email as giver_email,
    t.notes as notes, t.created_at as transfer_date
    FROM transfers as t
    LEFT JOIN coins as c on t.coin_id = c.id
    LEFT JOIN users AS u ON t.user_id = u.id
    LEFT JOIN users as g on t.giver_id = g.id
    ORDER BY coin_number ASC, transfer_date;
    SQL
  end

  def self.all_transfers_csv
    results = ActiveRecord::Base.connection.execute(all_transfers_query)
    attrs = TRANSFER_ATTRS
    CSV.generate(headers: true) do |csv|
      csv << attrs

      results.each do |tuple|
        csv << TRANSFER_ATTRS.map {|attr| tuple[attr] }
      end
    end
  end  

  def should_validate?
    new_record?
  end
  
  def user_full_name
    if self.user
      "#{self.user.first_name} #{self.user.last_name}" 
    else 
      ""
    end
  end

  def only_one_coin
    if self.user.coins.length >= 1
      errors.add(:transfer, "user already has a coin, select another user")
    end
  end

  def self.as_csv
    attrs = Transfer.new.attributes.keys
    CSV.generate(headers: true) do |csv|
      csv << attrs

      Transfer.find_each do |t|
        csv << attrs.map {|attr| t.send(attr.to_sym) }
      end
    end
  end

  def self.active_owners
    latest_transfers = Transfer.select('DISTINCT ON ("coin_id") *').order(:coin_id, created_at: :desc)
    latest_transfers.map do |transfer|
      user = transfer.user
      user.as_json.merge({
        "received" => transfer.created_at.in_time_zone('Eastern Time (US & Canada)'),
        "days_left" => transfer.days_left,
        "coin_number" => transfer.coin&.display_number,
        "notes" => transfer.notes
      })
    end
  end

  def self.active_owners_as_csv
    active_owners = self.active_owners
    attributes = ["id", "first_name","last_name", "email", "received", "days_left", "coin_number", "zip", "notes"]
    CSV.generate(headers: true) do |csv|
      csv << attributes

      active_owners.each do |owner|
        csv << attributes.map{ |attr| owner[attr] }
      end
    end
  end

  def self.vz_active_owners_as_csv
    active_owners = self.active_owners
    attributes = ["id", "first_name","last_name", "email", "received", "days_left", "coin_number", "zip", "notes"]
    CSV.generate(headers: true) do |csv|
      csv << attributes

      active_owners.each do |owner|
        if owner['email'].include? 'verizon'
          csv << attributes.map{ |attr| owner[attr] }
        end
      end
    end
  end

  def self.transfer_report_as_csv
    transfers = []
    Coin.find_each do |coin|
      coin.transfers.each.with_index do |transfer, idx|
         unless idx == 0
            user = transfer.user
            transfers.push({
              'id': transfer.id,
              'coin_id': transfer.coin_id,
              'coin_number': transfer.coin.try(:number),
              'created_at': transfer.created_at,
              'first_name': user.try(:first_name),
              'last_name': user.try(:last_name),
              'notes': transfer.try(:notes),
              'zip': user.try(:zip),
              'email': user.try(:email)})
          end
       end
    end
    attributes = ["id", "coin_id", "coin_number", "created_at", "first_name", "last_name", "notes", "zip", "email"]
    CSV.generate(headers: true) do |csv|
      csv << attributes

      transfers.each do |item|
        csv << [item[:id], item[:coin_id], item[:coin_number], item[:created_at], item[:first_name], item[:last_name], item[:notes], item[:zip], item[:email]]
      end
    end
  end

  def self.vz_transfer_report_as_csv
    transfers = []
    Coin.find_each do |coin|
      coin.transfers.each.with_index do |transfer, idx|
         unless idx == 0
            user = transfer.user
            if user.try(:email).include? 'verizon'
              transfers.push({
                'id': transfer.id,
                'coin_id': transfer.coin_id,
                'coin_number': transfer.coin.try(:number),
                'created_at': transfer.created_at,
                'first_name': user.try(:first_name),
                'last_name': user.try(:last_name),
                'notes': transfer.try(:notes),
                'zip': user.try(:zip),
                'email': user.try(:email)
              })
            end
          end
       end
    end
    attributes = ["id", "coin_id", "coin_number", "created_at", "first_name", "last_name", "notes", "zip", "email"]
    CSV.generate(headers: true) do |csv|
      csv << attributes

      transfers.each do |item|
        csv << [item[:id], item[:coin_id], item[:coin_number], item[:created_at], item[:first_name], item[:last_name], item[:notes], item[:zip], item[:email]]
      end
    end
  end

  def not_same_user
    if self.coin.owner === self.user
      errors.add(:transfer, "you can't transfer the coin to yourself")
    end
  end

  def is_coin_owner
    #should verify that the owner is the giver
  end

  def giver_vz
    User.select(:first_name, :last_name).where(id: self.giver_id).first
  end

  def user_vz
    User.select(:first_name, :last_name, :city, :state).where(id: self.user_id).first
  end

  def days_left
    begin
      days_passed = (((self.created_at.beginning_of_day + 60.days) - Time.now.beginning_of_day) / 86400).ceil
      if days_passed <= 0
        return 0
      end
      days_passed
    rescue
      nil
    end
  end

  def days_left_leader
    begin
      days_passed = (((self.created_at.beginning_of_day + 63.days) - Time.now.beginning_of_day) / 86400).ceil
      if days_passed <= 0
        return 0
      end
      days_passed
    rescue
      nil
    end
  end

end

