class Coin < ApplicationRecord
  has_many :transfers, dependent: :delete_all
  has_many :users, through: :transfers
  # validates_associated :users

  validates_uniqueness_of :number

  def owner
    latest_transfer.try(:user)
  end

  def display_number
    number.to_s.rjust(5, "0")
  end

  def previous_owner
    transfers.where("created_at is NOT NULL").order('created_at DESC').second.try(:user)
  end

  def latest_transfer
    transfers.where("created_at is NOT NULL").order('created_at DESC').first
  end

  def has_tranfers
    Transfer.where(coin_id: self.id).exists?
  end

  def original_owner
    transfers.where("created_at is NOT NULL").order('created_at ASC').first.try(:user)
  end

end