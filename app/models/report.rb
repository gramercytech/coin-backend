class Report

  def self.mail_daily_report
    # 'malcolm@gramercytech.com', 'jeremy@gramercytech.com'
    ['thomas.yancey@gramercytech.com','malcolm@gramercytech.com', 'jeremy@gramercytech.com'].each do |email|
      ReportMailer.daily_update(email).deliver_now
    end
  end

end