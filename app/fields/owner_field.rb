require "administrate/field/base"

class OwnerField < Administrate::Field::Base
  def to_s
    # data&.name&.full || data&.email
    data&.email
  end
end
