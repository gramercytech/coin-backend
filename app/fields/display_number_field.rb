require "administrate/field/base"

class DisplayNumberField < Administrate::Field::Base
  def to_s
    data.to_s.rjust(5, "0")
  end
end
