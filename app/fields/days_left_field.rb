require "administrate/field/base"

class DaysLeftField < Administrate::Field::Base
  def to_s
    data
  end
end
