
class ReportMailer < ApplicationMailer



  def daily_update(email)
    @transfer_count = Transfer.where('created_at > ?', 1.day.ago).count
    @contacts = Contact.where({resolved: false})
    @total_transfers = Transfer.where.not(giver_id: nil).count

    # attachments["all_transfers.csv"] = {
    #   mime_type: 'text/csv',
    #   content: Transfer.all_transfers_csv 
    # }

    mail( 
      :from => 'help@vzpurposecoin.com',
      :to => email,
      :subject => "Daily Transfers"
    )
  end

end
