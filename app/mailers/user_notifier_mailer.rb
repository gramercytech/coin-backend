class UserNotifierMailer < ApplicationMailer
  default :from => 'notifications@vzpurposecoin.com'

  def send_transfer_notification_email(transfer)
    @user = transfer.user
    @giver = User.find_by({id: transfer.giver_id})
    @coin = transfer.coin
    mail( :to => @user.email,
    :subject => "You've received a Verizon Purpose Coin!" )
  end

  def send_previous_user_notification_email(transfer, previous_user)
    @transfer = transfer
    @user = transfer.user
    @giver = User.find_by({id: transfer.giver_id})
    @coin = transfer.coin
    @previous_user = previous_user
    mail( :to => @previous_user.email,
    :subject => "Your Verizon Purpose Coin is on the move!" )
  end

  def support_request_email(contact)
    @contact = contact
    mail( :to => 'help@vzpurposecoin.com', :subject => "New Support Request" )
  end

  def send_test_transfer_notification_email(subject, user, giver_id)
    @user = user
    @giver = User.find_by({id: giver_id})
    mail( :to => @user.email,
    :subject => "You've received a Verizon Purpose Coin!" )
  end

  def send_days_left_email(user)
    @user = user
    @coin = @user.coin
    transfer = @coin.latest_transfer
    @days_left = transfer.days_left
    mail( :to => @user.email,
      :subject => "It's Time to Pass your Purpose Coin." )
  end

  def send_test_days_left_email(user)
    @user = user
    @coin = @user.coin
    transfer = @coin.latest_transfer
    @days_left = transfer.days_left
    mail( :to => @user.email,
      :subject => "It's Time to Pass your Purpose Coin.")
  end
end
