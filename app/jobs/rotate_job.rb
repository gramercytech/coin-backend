require "image_processing/mini_magick"
require 'open-uri'

class RotateJob < ApplicationJob
  queue_as :default
  include ImageProcessing::MiniMagick
  include Rails.application.routes.url_helpers

  def perform(transfer_id, degree)
    transfer = Transfer.find(transfer_id)
    image = transfer.files.last
    ext_path = Rails.application.routes.url_helpers.rails_blob_path(image, only_path: true)
    ext_end = ext_path.split('/')[-1]
    ext = ext_end.split(".")[-1]
    tempfile_name = "#{SecureRandom.hex(10)}.#{ext}"
    orig_path = "#{ Rails.env.development? ? 'http://localhost:3000' : 'https://api.vzpurposecoin.com'}#{ext_path}"
    begin
      Rails.logger.info("ORIG_PATH")
      Rails.logger.info(orig_path)
      Rails.logger.info("ORIG_PATH")
      File.open(tempfile_name, 'wb') {|f|
        Rails.logger.info("OPENED TEMPFILE")
        Rails.logger.info("-------")
        open(orig_path) {|of|
          Rails.logger.info("OPENED remote file #{orig_path}")
          f.write(of.read)
          Rails.logger.info("READ remote file #{orig_path}")
        }
        f.rewind
      }
      Rails.logger.info("----------")
      Rails.logger.info("have read")
      Rails.logger.info("----------")
      processed = ImageProcessing::MiniMagick.source(tempfile_name).rotate(degree ? degree : 90).call
      Rails.logger.info("----------")
      Rails.logger.info("attaching")
      Rails.logger.info("----------")
      transfer.files.attach(io: File.open(processed.path), filename: tempfile_name)
      Rails.logger.info("----------")
      Rails.logger.info("attached")

      if transfer.files.length > 0
        transfer.files[0].purge
      end  
      Rails.logger.info("Successfully made it to the end")
    rescue => e
      Rails.logger.info(e.message)
    ensure
      File.delete(tempfile_name) if File.exist?(tempfile_name)
    end

  end
end
