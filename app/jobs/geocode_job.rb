class GeocodeJob < ApplicationJob
  queue_as :default

  def perform(user_id)
    user = User.find(user_id)
    user.fetch_location_data
  end
end
