class TransfersController < ApplicationController
  before_action :set_transfer, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_super_user!, except: [:create, :upload_file]
  before_action :set_cors_headers, only: [:update]
  skip_before_action :verify_authenticity_token

  include Rails.application.routes.url_helpers

  def set_cors_headers

  end

  def index
    @transfers = Transfer.all
    respond_to do |format|
      format.html { render 'tranfers/index' }
      format.csv { send_data Transfer.as_csv }
    end
  end

  def show
  end

  def new
    @transfer = Transfer.new
  end

  def edit
  end

  def activityfeed
    transfers = Transfer.joins("left join users on user_id = users.id").select('transfers.*, users.id, users.first_name').order('created_at DESC')
    transfers_with_givers = transfers.joins('left join users as givers on transfers.giver_id = givers.id left join coins on transfers.coin_id=coins.id')
                                     .select("transfers.id as id, CONCAT(givers.first_name,' ', givers.last_name) as giver_name, coins.number as coin_number")
    @activity = transfers_with_givers
    @markers = User.where("latitude IS NOT NULL AND longitude IS NOT NULL").pluck('latitude', 'longitude', 'first_name', 'id')
  end

  def stats
    transfers = Transfer.joins("left join users on user_id = users.id").select('transfers.*, users.id, users.first_name').order('created_at DESC')
    transfers_with_givers = transfers.joins('left join users as givers on transfers.giver_id = givers.id').select('transfers.id as id, givers.first_name as giver_name')
    @totalTransfers = transfers_with_givers
    # @totalToday = Transfer.where("created_at >= ?", Time.zone.now.beginning_of_day)
    @totalToday = Transfer.where("created_at >= ?", Date.today)
    @totalYesterday = Transfer.where("DATE(created_at) = ?", Date.today-1)
    @passedOnce = Transfer.group(:coin_id).distinct.having('count(coin_id) = 1').count.count
    @passedTwice = Transfer.group(:coin_id).distinct.having('count(coin_id) = 2').count.count
    @passedThreeOrMore = Transfer.group(:coin_id).distinct.having('count(coin_id) > 3').count.count
  end

  def active_owners

    respond_to do |format|
      format.csv {
        send_data Transfer.active_owners_as_csv
      }
      format.html {
        @active_owners = Transfer.active_owners
        render 'transfers/active_owners'
      }
    end
  end

  def vz_active_owners
    respond_to do |format|
      format.csv {
        send_data Transfer.vz_active_owners_as_csv
      }
    end
  end

  def transfer_report
    respond_to do |format|
      format.csv {
        send_data Transfer.transfer_report_as_csv
      }
    end
  end

  def vz_transfer_report
    respond_to do |format|
      format.csv {
        send_data Transfer.vz_transfer_report_as_csv
      }
    end
  end

  def create
    user = User.where("lower(email) = ?", params[:email].downcase).first

    unless user
      user = User.new({
        first_name: params[:first_name],
        last_name: params[:last_name],
        email: params[:email].downcase
      })

      unless user.save
        render json: @user.errors, status: :unprocessable_entity
        return
      end

    end

    coin = Coin.find_by(id: params[:coin_id])

    unless coin
      render json: "coin not found", status: :unprocessable_entity
      return
    end

    @transfer = Transfer.new({
      giver_id: coin.owner.id,
      user: user,
      coin: coin,
      notes: params[:notes]
    })

    respond_to do |format|
      if @transfer.save
        UserNotifierMailer.send_transfer_notification_email(@transfer).deliver_now
        
        coin.users.where.not(id: [@transfer.giver_id, user.id]).find_each {|previous_owner|
          UserNotifierMailer.send_previous_user_notification_email(@transfer, previous_owner).deliver_later
        }
        
        format.html { redirect_to @transfer, notice: 'Transfer was successfully created.' }
        format.json { render :show, status: :created, location: @transfer }
      else
        format.html { render :new }
        format.json { render json: @transfer.errors, status: :unprocessable_entity }
      end
    end
  end

  def upload_file
    @transfer = Transfer.find_by(id: params[:id])
    if @transfer.files.attach(params[:files])
      render json: rails_blob_path(@transfer.files.first, disposition: "attachment", only_path: true), status: :ok
    else
      render json: @transfer.errors, status: :unprocessable_entity
    end
  end

  def update
    respond_to do |format|
      if @transfer.update(transfer_params)
        format.html { redirect_to @transfer, notice: 'Transfer was successfully updated.' }
        format.json { render :show, status: :ok, location: @transfer }
      else
        format.html { render :edit }
        format.json { render json: @transfer.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @transfer.destroy
    respond_to do |format|
      format.html { redirect_to transfers_url, notice: 'Transfer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_transfer
      @transfer = Transfer.find(params[:id])
    end

    def transfer_params
      params.require(:transfer).permit(:notes, :user_id, :coin_id)
    end
end
