module Admin
  class ReportsController < ApplicationController
    
    def index
      render '/reports/index'
    end

    def send_daily_update
      ReportMailer.daily_update.deliver_now
      redirect_back(fallback_location: root_path)
    end

  end
end