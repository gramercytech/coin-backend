module Admin
  class CoinsController < Admin::ApplicationController
    # Overwrite any of the RESTful controller actions to implement custom behavior
    # For example, you may want to send an email after a foo is updated.
    #
    # def update
    #   foo = Foo.find(params[:id])
    #   foo.update(params[:foo])
    #   send_foo_updated_email
    # end

    # def index
    #   search_term = params[:search].to_s.strip
    #   resources = Administrate::Search.new(scoped_resource,
    #                                        dashboard_class,
    #                                        search_term).run
    #   resources = apply_collection_includes(resources)
    #   resources = order.apply(resources)
    #   resources = resources.page(params[:page]).per(records_per_page)
    #   page = Administrate::Page::Collection.new(dashboard, order: order)

    #   render locals: {
    #     resources: resources,
    #     search_term: search_term,
    #     page: page,
    #     show_search_bar: show_search_bar?,
    #   }
    # end

    def create
      @coin = Coin.new(coin_params)
      
      # Don't create user if the field isn't filled out or the coin didn't save
      @user = User.where(email: user_email_params[:user_email]).first_or_create if @coin.save && user_email_params[:user_email]
      if @user
        Transfer.create(
          user_id: @user.id,
          coin_id: @coin.id, 
        )
      end

      if @coin.valid?
        redirect_to(
          admin_coin_path(@coin),
          notice: translate_with_resource("create.success"),
        )
      else
        render :new, locals: {
          page: Administrate::Page::Form.new(dashboard, @coin),
          notice: @coin.errors.full_messages.to_s
        }
      end
    end

    # Override this method to specify custom lookup behavior.
    # This will be used to set the resource for the `show`, `edit`, and `update`
    # actions.
    #
    # def find_resource(param)
    #   Foo.find_by!(slug: param)
    # end

    # Override this if you have certain roles that require a subset
    # this will be used to set the records shown on the `index` action.
    #
    # def scoped_resource
    #  if current_user.super_admin?
    #    resource_class
    #  else
    #    resource_class.with_less_stuff
    #  end
    # end

    # See https://administrate-prototype.herokuapp.com/customizing_controller_actions
    # for more information



    private

    def user_email_params
      params.require(:coin).permit(:user_email)
    end

    def coin_params
      params.require(:coin).permit(:number)
    end

    def show_search_bar?
      dashboard.attribute_types_for(
        dashboard.all_attributes,
      ).any? { |_name, attribute| attribute.searchable? }
    end
  end
end
