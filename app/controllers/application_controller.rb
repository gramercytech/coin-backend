class ApplicationController < ActionController::Base

  class ForbiddenError < StandardError
  end

  rescue_from ApplicationController::ForbiddenError do |exception|
    head :forbidden
  end

  def authenticate_super_user_unless_json
    authenticate_super_user! unless request.format.json?
  end

  def authenticate_vz_web_call
    Rails.logger.info("------")
    Rails.logger.info("request domain auth vz web ---- #{request.headers['origin']}")
    Rails.logger.info("------") 
    unless params[:secret_key] && params[:secret_key] == "967efb08b60f3555d8bc"
      raise ForbiddenError
    end
    true
  end

  def check_for_coin
    @user = User.find(session[:current_coin])
  end

end
