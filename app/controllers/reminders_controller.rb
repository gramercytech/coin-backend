class RemindersController < ApplicationController
  before_action :set_reminder, only: [:show, :edit, :update, :destroy]

  # GET /reminders
  # GET /reminders.json
  def current_users
    query_string = <<-HEREDOC
    select t.id, t.user_id, t.coin_id
    from (
      select coin_id, max(created_at) as maxcreated_at 
      from transfers group by coin_id
    ) as x inner join transfers as t on t.coin_id = x.coin_id and t.created_at = x.maxcreated_at;
    HEREDOC




    res = ActiveRecord::Base.connection.execute(query_string)

    if params[:filter] == '1'
      new_query = ActiveRecord::Base.connection.execute(
        <<~HEREDOC
          select * from users
          WHERE id in (#{res.pluck("id").join(",")})
          AND NOT EXISTS (
              SELECT 1 FROM reminders
              WHERE reminders.user_id = users.id
          )
          HEREDOC
      )
      @transfers = Transfer.where({id: res.pluck("id")}).joins(:user).where('transfers.user_id in (?)', new_query.pluck('id')).order(created_at: 'asc').paginate(page: params[:page], per_page: 30)
      @filtered = true
    else 
      @transfers = Transfer.where({id: res.pluck("id")}).left_joins(:user).order(created_at: 'asc').paginate(page: params[:page], per_page: 30)
    end
    
    render 'current_users'

  end

  def index
    @reminders = Reminder.all
  end

  def create
    user_id = params["reminder"]["user_id"]
    if !user_id
      raise "no user id provided"
    end

    user = User.find(user_id)

    mailer_res = UserNotifierMailer.send_days_left_email(user).deliver_now
    
    if !mailer_res
      puts mailer_res
      raise 'Error sending'
    end

    if mailer_res
      @reminder = Reminder.new({user: user})
    end

    respond_to do |format|
      if @reminder.save
        format.html {
          redirect_back(fallback_location: root_path)
        }
      else
        format.html { render :new }
        format.json { render json: @reminder.errors, status: :unprocessable_entity }
      end
    end
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reminder
      @reminder = Reminder.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reminder_params
      params.require(:reminder).permit(:user_id)
    end
end
