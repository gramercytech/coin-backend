# TRANSFER_ATTRS = ['coin_number', 'user_first_name', 'user_last_name', 'user_email', 'user_zip', 'giver_first_name', 'giver_email', 'notes', 'transfer_date']
class ReportsController < ApplicationController

  def index
    render layout: '/admin/application'
  end

  def all_transfers

    c = Transfer.all_transfers_csv

    respond_to do |format|
      format.csv { send_data c }  
    end

  end

  def days_since
    
    days_since = params[:days_since]

    if !days_since
      redirect_back(fallback_location: root_path)
      return
    end

    where_clause = "WHERE t.created_at > NOW() - INTERVAL '#{days_since} days'"

    query = <<~SQL
      SELECT c.number as coin_number,
      u.first_name as user_first_name, u.last_name as user_last_name, u.email as user_email, u.zip as user_zip, 
      g.first_name as giver_first_name, g.last_name as giver_last_name, g.email as giver_email,
      t.notes as notes, t.created_at as transfer_date
      FROM transfers as t
      LEFT JOIN coins as c on t.coin_id = c.id
      LEFT JOIN users AS u ON t.user_id = u.id
      LEFT JOIN users as g on t.giver_id = g.id
      #{where_clause}
      ORDER BY coin_number ASC, transfer_date;
    SQL

    results = ActiveRecord::Base.connection.execute(query)
    attrs = Transfer::TRANSFER_ATTRS
    c = CSV.generate(headers: true) do |csv|
      csv << attrs

      results.each do |tuple|
        csv << Transfer::TRANSFER_ATTRS.map {|attr| tuple[attr] }
      end
    end
    
    respond_to do |format|
      format.csv { send_data c }  
    end
  end    

  def transfers_past_seven
    query = <<~SQL
    SELECT c.number as coin_number,
    u.first_name as user_first_name, u.last_name as user_last_name, u.email as user_email, u.zip as user_zip, 
    g.first_name as giver_first_name, g.last_name as giver_last_name, g.email as giver_email,
    t.notes as notes, t.created_at as transfer_date
    FROM transfers as t
    LEFT JOIN coins as c on t.coin_id = c.id
    LEFT JOIN users AS u ON t.user_id = u.id
    LEFT JOIN users as g on t.giver_id = g.id
    WHERE t.created_at > NOW() - INTERVAL '7 days'
    ORDER BY transfer_date;
    SQL
    
    results = ActiveRecord::Base.connection.execute(query)
    attrs = Transfer::TRANSFER_ATTRS
    c = CSV.generate(headers: true) do |csv|
      csv << attrs

      results.each do |tuple|
        csv << Transfer::TRANSFER_ATTRS.map {|attr| tuple[attr] }
      end
    end
    
    respond_to do |format|
      format.csv { send_data c }  
    end
  end  

  def users
    attrs = User.new.attributes.keys

    c = CSV.generate(headers: true) do |csv|
      csv << attrs
      User.find_each {|user| csv << attrs.map {|attr| user[attr] } }
    end

    respond_to do |format|
      format.csv { send_data c } 
    end
  end

end