class CoinsController < ApplicationController
  before_action :set_coin, only: [:edit, :update, :destroy]
  before_action :authenticate_super_user!, except: [:all_transfers, :show, :update_owner, :transfers, :index, :filters, :vz_web, :vz_web_show]
  skip_before_action :verify_authenticity_token
  

  def index
    @coins = Coin.all
  end

  def filters
    authenticate_vz_web_call
    render json: Coin.count
  end

  def vz_web
    authenticate_vz_web_call
    @coins = Coin.where('number BETWEEN ? AND ?', params[:first_coin], params[:last_coin]).joins(:transfers).eager_load(:transfers).order(number: :asc)
    render "/coins/coins_vz_web"
  end

  def vz_web_show
    authenticate_vz_web_call
    @coin = Coin.find_by(id: params[:id])
    respond_to do |format|
      if @coin
        session[:current_coin] = @coin.number
        format.html { render :show }
        format.json { render '/coins/coin_vz_web', status: :created, location: @coin }
      else
        format.html {
          redirect_back(fallback_location: root)
        }
        format.json { render json: "not found", status: :not_found }
      end
    end
  end

  def transfers
    coin = Coin.find(params[:id])
    @transfers = coin.transfers.eager_load(:user)
  end

  def all_transfers
    # @transfers = Transfer.all
    transfers = Transfer.joins("left join users on user_id = users.id").select('transfers.*, users.id, users.first_name, users.latitude, users.longitude').order('created_at DESC')
    transfers_with_givers = transfers.joins('left join users as givers on transfers.giver_id = givers.id').select('transfers.id as id, givers.first_name as giver_name')
    render json: transfers_with_givers.to_json
  end

  def show
    @coin = Coin.find_by(number: params[:id])
    respond_to do |format|
      if @coin
        session[:current_coin] = @coin.number
        format.html { render :show }
        format.json { render :show, status: :created, location: @coin }
      else
        format.html {
          redirect_back(fallback_location: root)
        }
        format.json { render json: "not found", status: :not_found }
      end
    end
  end

  def new
    @coin = Coin.new
  end

  def update_owner
    coin = Coin.find_by(id: params[:coin_id])

    unless coin
      render json: "coin not found", status: 422
      return
    end

    @user = coin.owner
    @user.email = params[:email].downcase
    @user.first_name = params[:first_name]
    @user.last_name = params[:last_name]
    @user.zip = params[:zip]
    @user.latitude = params[:latitude]
    @user.longitude = params[:longitude]
    @user.formatted_address = params[:formatted_address]
    @user.verified = true


    if @user.save
      render("users/show.json", status: :created)
    else
      render json: "unable to update user", status: 422
    end
  end

  def edit
  end

  def create
    @coin = Coin.new(coin_params)

    respond_to do |format|
      if @coin.save
        format.html { redirect_to @coin, notice: 'Coin was successfully created.' }
        format.json { render :show, status: :created, location: @coin }
      else
        format.html { render :new }
        format.json { render json: @coin.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @coin.update(coin_params)
        format.html { redirect_to @coin, notice: 'Coin was successfully updated.' }
        format.json { render :show, status: :ok, location: @coin }
      else
        format.html { render :edit }
        format.json { render json: @coin.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @coin.destroy
    respond_to do |format|
      format.html { redirect_to coins_url, notice: 'Coin was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def test_error
    throw 'test'
  end

  private
    def set_coin
      @coin = Coin.find(params[:id])
    end

    def coin_params
      params.require(:coin).permit(:number)
    end
end
