class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_super_user!, only: [:create, :show, :destroy]
  skip_before_action :verify_authenticity_token
  LETTERS = [ 'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z' ]

  def first_name
    render json: User.select(:first_name).limit(100).order(id: :asc).pluck(:first_name)
  end

  def index
    @users = User.all
  end

  def vz_web_show
    authenticate_vz_web_call
    if !LETTERS.include?(params[:letter])
      render json: {message: 'forbidden'}, status: 403
      return
    end
    @users = User.where("regexp_replace(last_name, '^.* ', '') ILIKE ?", "#{params[:letter]}%")
                 .order("regexp_replace(last_name, '^.* ', '')")
                 .eager_load(:coins, :transfers)
    render '/users/index_vz_web'
  end

  def user_by_email
    @user = User.where("lower(email) = ?", params[:email].downcase).first
    @coin = Coin.where('number', params[:coin])
    if @user
      # if session[:current_coin].present?
        render "users/show.json", status: :ok
    else
      render json: "user not found", status: :not_found
    end
  end

  def show
  end

  def new
    @user = User.new
  end

  def edit
  end

  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:email, :first_name, :last_name, :location)
    end
end
