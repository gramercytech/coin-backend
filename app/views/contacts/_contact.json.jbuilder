json.extract! contact, :id, :reason, :explanation, :email, :created_at, :updated_at
json.url contact_url(contact, format: :json)
