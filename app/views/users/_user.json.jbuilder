json.extract! user, :id, :email, :first_name, :location, :created_at, :updated_at, :verified
json.coin user.coin
json.is_owner user.coin && user.coin.owner == user
json.url user_url(user, format: :json)
