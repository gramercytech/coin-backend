json.extract! user, :id, :first_name, :last_name, :location, :city, :state, :created_at, :updated_at, :verified
json.coin user.coin
if user.transfers.first
  json.transfer do
    transfer = user.transfers.first 
    json.extract! transfer, :id, :created_at, :giver_id, :notes
    if transfer.files.first
      attachment = transfer.files.first
      if attachment.image?
        json.filepath Rails.application.routes.url_helpers.rails_blob_path(transfer.files.first, only_path: true)
        json.filetype 'image'
      elsif attachment.video? && attachment.previewable?
        json.filepath Rails.application.routes.url_helpers.rails_blob_path(transfer.files.first, only_path: true)
        json.filetype 'video/mp4'
      else 
        json.filepath nil
        json.filetype nil
      end
    else
      json.filepath nil
      json.filetype nil
    end
      
  end
end
  
# json.is_owner user.coin && user.coin.owner == user
