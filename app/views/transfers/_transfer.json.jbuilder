json.extract! transfer, :id, :notes, :user_id, :coin_id, :created_at, :updated_at
json.user transfer.user
json.days_left transfer.days_left || ""
json.url transfer_url(transfer, format: :json)
