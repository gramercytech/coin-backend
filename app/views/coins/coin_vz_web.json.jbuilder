
json.extract! @coin, :id, :number, :created_at, :updated_at
json.owner do
  json.id @coin.owner.try(:id)
  json.first_name @coin.owner.try(:first_name)
  json.last_name @coin.owner.try(:last_name)
end
json.vz_transfers @coin.transfers.order(created_at: :desc) do |transfer|
  json.extract! transfer, :id, :created_at
  json.giver transfer.giver_vz
  json.user transfer.user_vz
  json.notes transfer.notes
  if transfer.files.first
    attachment = transfer.files.first
    if attachment.image?
      json.filepath Rails.application.routes.url_helpers.rails_blob_path(transfer.files.first, only_path: true)
      json.filetype 'image'
    elsif attachment.video? && attachment.previewable?
      json.filepath Rails.application.routes.url_helpers.rails_blob_path(transfer.files.first, only_path: true)
      json.filetype 'video/mp4'
    else 
      json.filepath nil
      json.filetype nil
    end
  else
    json.filepath nil
    json.filetype nil
  end
end
