json.extract! coin, :id, :number, :created_at, :updated_at
if coin.owner
  json.owner do
    if coin.owner.nil?
      json.null! # or json.nil!
    else
      json.id coin.owner.id
      json.first_name coin.owner.first_name
      json.last_name coin.owner.last_name
      json.city coin.owner.city
      json.state coin.owner.state
      json.created_at coin.owner.created_at
      transfer = coin.owner.transfers.first
      json.transfer do 
        if transfer && transfer.files.first
          attachment = transfer.files.first
          if attachment.image?
            json.filepath Rails.application.routes.url_helpers.rails_blob_path(attachment, only_path: true)
            json.filetype 'image'
          elsif attachment.video? && attachment.previewable?
            json.filepath Rails.application.routes.url_helpers.rails_blob_path(attachment, only_path: true)
            json.filetype 'video/mp4'
          else 
            json.filepath nil
            json.filetype nil
          end
          json.notes  transfer.notes
        else
          json.filepath nil
          json.filetype nil
          json.notes transfer.notes
        end      
      end
    end
  end
end
json.transfers coin.transfers.count
# if coin.transfers.length > 0
#   if coin.transfers.length === 1
#     json.days_left_current coin.transfers.last.try(:days_left_leader)
#   end
#   if coin.transfers.length > 1
#     json.days_left_current coin.transfers.last.try(:days_left)
#   end
# end
# json.previous_owner do
#   if coin.previous_owner.nil?
#     json.null! # or json.nil!
#   else
#     json.id coin.previous_owner.id
#     json.first_name coin.previous_owner.first_name
#   end
# end
