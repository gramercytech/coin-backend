json.extract! coin, :id, :number, :created_at, :updated_at
json.original_owner do
  if coin.original_owner.nil?
    json.null! # or json.nil!
  else
    json.id coin.original_owner.id
    json.first_name coin.original_owner.first_name
  end
end
json.owner do
  if coin.owner.nil?
    json.null! # or json.nil!
  else
    json.id coin.owner.id
    json.first_name coin.owner.first_name
    json.last_name coin.owner.last_name
    json.created_at coin.owner.created_at
  end
end
json.transfers coin.transfers
if coin.transfers.length > 0
  if coin.transfers.length === 1
    json.days_left_current coin.transfers.last.try(:days_left_leader)
  end
  if coin.transfers.length > 1
    json.days_left_current coin.transfers.last.try(:days_left)
  end
end
json.previous_owner do
  if coin.previous_owner.nil?
    json.null! # or json.nil!
  else
    json.id coin.previous_owner.id
    json.first_name coin.previous_owner.first_name
  end
end
