json.transfers @transfers.find_each do |transfer|
  json.user do
  	json.id transfer.user.id
  	json.first_name transfer.user.first_name
  	json.last_name ""
    json.formatted_address transfer.user.formatted_address
  end
  json.created_at transfer.created_at.strftime("%m/%d/%Y")
end
