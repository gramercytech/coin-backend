# Preview all emails at http://localhost:3000/rails/mailers/user_notifier_mailer
class UserNotifierMailerPreview < ActionMailer::Preview
  def send_transfer_notification_email
    UserNotifierMailer.send_transfer_notification_email(Transfer.all.sample)
  end

  def days_left
    UserNotifierMailer.send_days_left_email(Coin.all.sample.owner)
  end
end
