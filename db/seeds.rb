require 'faker'

# unless Coin.all.any?
#   300.times { |i| Coin.create(number: i) }
# end


# unless User.all.any?
#   300.times do |i|
#     user = User.create(
#       email: Faker::Internet.email,
#       name: Faker::Name.first_name+' '+Faker::Name.last_name,
#     )
#     coin = Coin.find_by_number(i+1)
#     if coin && user
      # Transfer.create(
      #   user_id: user.id,
      #   coin_id: coin.id, 
      #   notes: Faker::Lorem.paragraph
      # )
#     end
#   end
# end

u1 = User.create({first_name: "Thomas", last_name: "Yancey", email: "thomas.yancey@gramercytech.com" })
u2 = User.create({first_name: "Drew", last_name: "Reynolds", email: "drew@gramercytech.com" })
u3 = User.create({first_name: "Jeremy", last_name: "Patuto", email: "jeremy@gramercytech.com" })
u4 = User.create({first_name: "Sophie", last_name: "Adams", email: "sophie@gramercytech.com" })
u5 = User.create({first_name: "Tanner", last_name: "Van Kampen", email: "tanner@gramercytech.com" })

users = [u1, u2, u3, u4, u5]
100.times do |_|
  users << User.create({
    first_name: Faker::Name.first_name,
    last_name: Faker::Name.last_name,
    email: Faker::Internet.email
  })
end

users.each.with_index do |user, i|
  coin = Coin.create({ number: i + 1 })
  user.coins << coin
end

SuperUser.create(
  email: 'drew@gramercytech.com',
  password: 'testing123',
  password_confirmation: 'testing123'
)

SuperUser.create(
  email: 'thomas.yancey@gramercytech.com',
  password: 'testing123',
)


# first_name: Faker::Name.first_name,
# last_name: Faker::Name.last_name,

# title: Faker::Job.title,
# division: Faker::Job.field,