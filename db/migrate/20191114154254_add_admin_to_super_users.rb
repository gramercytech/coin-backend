class AddAdminToSuperUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :super_users, :admin, :boolean, default: false
  end
end
