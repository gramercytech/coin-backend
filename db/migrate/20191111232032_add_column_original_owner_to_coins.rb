class AddColumnOriginalOwnerToCoins < ActiveRecord::Migration[6.0]
  def change
    add_reference :coins, :original_owner, index: { unique: true }
  end
end
