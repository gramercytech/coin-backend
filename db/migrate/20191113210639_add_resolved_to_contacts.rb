class AddResolvedToContacts < ActiveRecord::Migration[6.0]
  def change
    add_column :contacts, :resolved, :boolean, default: false
  end
end
