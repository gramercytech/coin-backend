class CreateCoins < ActiveRecord::Migration[6.0]
  def change
    create_table :coins do |t|
      t.integer :number

      t.timestamps
    end
    add_index :coins, :number, unique: true
  end
end
