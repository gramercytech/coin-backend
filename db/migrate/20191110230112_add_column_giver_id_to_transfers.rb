class AddColumnGiverIdToTransfers < ActiveRecord::Migration[6.0]
  def change
    add_column :transfers, :giver_id, :integer
  end
end
