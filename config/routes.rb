class RestrictedListConstraint
    
  def initialize
    @allowed_subdomains = ['superadmin', 'verizoncoin-env-4', 'verizoncoin-env-4.us-east-1']
    @allowed_subdomains << 'ngrok.io' if Rails.env.development?
    
    puts "INITIALIZED"
  end
  
  def matches?(request)
    puts "REQUEST SUBDOMAIN - #{request.subdomain}"
    if Rails.env.development?
      request.domain == 'localhost' || request.domain == 'ngrok.io'
    else
      @allowed_subdomains.include?(request.subdomain)
    end
  end

end

Rails.application.routes.draw do
  mount LetterOpenerWeb::Engine, at: "/letter_opener" if Rails.env.development?

  constraints RestrictedListConstraint.new do
    require 'sidekiq/web'
    mount Sidekiq::Web => '/sidekiq'
    devise_for :super_users, :skip => [:registrations]
    namespace :admin do
      resources :coins
      resources :transfers
      resources :users
      resources :contacts
      root to: 'coins#index'
      get '/reports', to: 'reports#index', as: 'reports_path'
      get '/reports/send_daily_update', to: 'reports#send_daily_update', as: 'daily_update'
      get 'transfers/:id/edit_images', to: 'transfers#edit_images'
      post 'transfers/:id/rotate', to: 'transfers#rotate'
    end

    get '/reminders/current_users', to: 'reminders#current_users'
    get '/reports/all_transfers', to: 'reports#all_transfers'
    get '/reports/transfers_past_seven', to: 'reports#transfers_past_seven'
    get '/reports/days_since', to: 'reports#days_since', as: 'days_since'
    get '/reports/users', to: 'reports#users'
    
    resources :reminders, only: [:create]

  end

  get '/coins/filters', to: 'coins#filters'
  get '/coins/vz_web', to: 'coins#vz_web'
  get '/coins/:id/vz_web', to: 'coins#vz_web_show'
  get '/users/vz_web', to: 'users#vz_web_show'

  get '/coins/all_transfers', to: 'coins#all_transfers', as: 'all_transfers'
  
  resources :transfers
  post 'transfers/:id/upload_file', to: 'transfers#upload_file', as: 'upload_file'
  get '/active_owners', to: 'transfers#active_owners', as: 'active_owners'
  get '/vz_active_owners', to: 'transfers#vz_active_owners', as: 'vz_active_owners'
  get '/transfer_report', to: 'transfers#transfer_report', as: 'transfer_report'
  get '/vz_transfer_report', to: 'transfers#vz_transfer_report', as: 'vz_transfer_report'
  get '/activityfeed', to: 'transfers#activityfeed', as: 'activityfeed'
  get '/stats', to: 'transfers#stats', as: 'stats'

  resources :coins
  post '/coins/update_owner', to: 'coins#update_owner', as: 'update_owner'
  get '/coins/:id/transfers', to: 'coins#transfers', as: 'coin_transfers'
  get '/coins/test_error', to: 'coins#test_error', as: 'test_error'

  resources :users
  get '/user_by_email', to: 'users#user_by_email', as: 'user_by_email'
  get 'first_names', to: 'users#first_name', as: 'users_first_name'

  resources :contacts
  root :to => redirect('/admin/coins')
end
