if Rails.env.development?
  Sidekiq.configure_server do |config|
    config.redis = { url: ENV.fetch('SIDEKIQ_REDIS_URL', 'redis://127.0.0.1:6379/1') }
  end

  Sidekiq.configure_client do |config|
    config.redis = { url: ENV.fetch('SIDEKIQ_REDIS_URL', 'redis://127.0.0.1:6379/1') }
  end
end

if Rails.env.production?
  Sidekiq.configure_server do |config|
    config.redis = { url: 'redis://vzcoin.qbqzt8.ng.0001.use1.cache.amazonaws.com:6379' }
  end

  Sidekiq.configure_client do |config|
    config.redis = { url: 'redis://vzcoin.qbqzt8.ng.0001.use1.cache.amazonaws.com:6379' }
  end
end  