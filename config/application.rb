require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module CoinBackend
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0

    config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins '*'
        resource '*', headers: :any, methods: [:get, :post, :options, :patch]
      end
    end

    if Rails.env.production?
      Raven.configure do |config|
        config.dsn = 'https://6b4aa51b6a8943d884e8c5bf88777394:f7b7aceee58c4ac9a4cf7f2e62cef5dc@sentry.io/1823002'
      end
    end

    config.assets.paths << Rails.root.join("app", "assets", "fonts")

    config.active_job.queue_adapter = :sidekiq

    # Settings in config/environments/* take precedence over those specified here.
  # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
  end
end
