namespace :clear_data do
  desc "Clear the Coins, Users and Transfers in the DB"
  task go: :environment do
    User.all.destroy_all
    Coin.all.destroy_all
    Transfer.all.destroy_all
  end
end
