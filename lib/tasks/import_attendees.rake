require 'csv'

namespace :import_attendees do
  desc "Import Verizon Event Attendees"
  task go: :environment do
    csv_text = File.read(Rails.root.join('lib', 'assets', 'attendees-randomized.csv'))
    csv = CSV.parse(csv_text, :headers => true)
    attendees_count = csv.size
    csv.each_with_index do |row, i|
      u = User.where(
        email: row["Email Address"]
      ).first_or_create
      u.update(name: "#{row["First Name"]} #{row["Last Name"]}")
      print "\r%-20s" % "Imported #{i} of #{attendees_count} attendees"
    end

    puts 'Attendees Imported, Starting Coin Assignment'

    Coin.all.destroy_all #Clear all Coins so we know that the same users will always be assigned the correct number

    hans = User.find_by_email('hans.vestberg@one.verizon.com')
    c = Coin.where(number: 1).first_or_create
    Transfer.create(user_id: hans.id, coin_id: c.id)

    csv.each_with_index do |row, i| # Use the same CSV each_with_index instead of User.all so the order is the same
      u = User.find_by_email(row["Email Address"] )
      next if u.transfers.any? #skip Hans
      coin = Coin.create({ number: i+2 }) #+2 since #1 has already been assigned
      u.coins << coin
      print "\r%-20s" % "Added #{i} Coins to attendees"
    end
  end
end